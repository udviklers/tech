class HomeController < ApplicationController
  def main
  end
  	
  
  def index
    if request.post?
      # The bottom line is that only the logged in user (admin) can access the Finder
      # The Session Name Must Be The Same As The Name You Use On Your System
      # The Logon Process Will Be If You Can Remove This Line
      if session[:user_id]
        # example file with the name you want under the terms you can create the public folder, 'uploads' was created in the form of
        # File.join(Rails.public_path, 'uploads', "/*")
        # Files you want to be listed as a parameter
        # server address,
        # post parameters,
        # Parameter Hash in the hash
        # :max_file_size = indicates the size of the file to be loaded (byte)
        # :allowed_mime = the file types that you want to allow extra
        # :disallowed_mime = disallowed file types
        render text: Fcfinder::Connector.new(File.join(Rails.public_path, 'uploads', "/*"), request.env["HTTP_HOST"], params[:fcfinder],
                                             {
                                                 :max_file_size => 1_000_000,
                                                 :allowed_mime => {'pdf' => 'application/pdf'},
                                                 :disallowed_mime => {}
                                             }).run, :layout => false
      # If Not Logged On
      #( session[:user_id] if you must remove the else part of the if statement block is removed)
      else
      # if the value is null is blocking access session_id, keep your files safe.
        render :text => "Access not allowed!".to_json, :layout => false
      end
    else
      render :layout => false
    end
  end


  #to download the file
  def download
    # 'uploads' folder under the public folder part again.
    send_file File.join(Rails.public_path,'uploads',params[:path].split(":").join("/")+"."+params[:format])
  end
end
